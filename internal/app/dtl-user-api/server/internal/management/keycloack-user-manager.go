package management

import (
	"context"
	"google.golang.org/appengine/memcache"
)

const TokenKey = "API_TOKEN"

type KeycloakUserManager struct {
}

func (kcm *KeycloakUserManager) callAdminAPI() {

}

func (kcm *KeycloakUserManager) getAdminAPIToken() {
	apiToken, err := memcache.Get(context.Background(), TokenKey)
	if err != nil || apiToken.Key == "" {

	}
}
