package models

import (
	"bitbucket.org/eunanibus/dtl-user-api/proto/auth"
	"time"
)

type NewUser struct {
	email          string
	username       string
	enabled        bool
	userAttributes map[string]interface{}
}

func GenerateNewUserFromRequest(req *auth.UserRegistrationRequest) *NewUser {
	newUser := &NewUser{
		email:          req.GetEmail(),
		username:       req.GetEmail(),
		enabled:        true,
		userAttributes: generateUserAttributes(req),
	}
	return newUser
}

func generateUserAttributes(req *auth.UserRegistrationRequest) map[string]interface{} {
	return map[string]interface{}{
		"agreed":     time.Now().UnixNano() / int64(time.Millisecond),
		"newsletter": req.GetNewsletter(),
		"gamertag":   req.GetGamertag(),
		"locale":     req.GetLang(),
		"cc":         req.GetCc(),
	}
}
