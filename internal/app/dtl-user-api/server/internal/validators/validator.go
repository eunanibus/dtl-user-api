package validators

import (
	"bitbucket.org/eunanibus/dtl-user-api/proto/auth"
	"errors"
	"regexp"
)

const ValidEmailRegex = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
const ValidPasswordRegex = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,32})"

type RegistrationValidator struct {
}

func (v *RegistrationValidator) ValidateRegistrationRequest(req *auth.UserRegistrationRequest) error {
	if req == nil {
		return errors.New("invalid request structure")
	}

	if err := v.validateEmail(req); err != nil {
		return err
	}

	if err := v.validatePassword(req); err != nil {
		return err
	}

	if err := v.validateTnc(req); err != nil {
		return err
	}

	return nil
}

func (v *RegistrationValidator) validateEmail(req *auth.UserRegistrationRequest) error {
	return v.validateAndMatch(req.GetEmail(), ValidEmailRegex, "email required", "invalid email address")
}

func (v *RegistrationValidator) validatePassword(req *auth.UserRegistrationRequest) error {
	return v.validateAndMatch(req.GetPassword(), ValidPasswordRegex, "password required", "invalid password")
}

func (v *RegistrationValidator) validateTnc(req *auth.UserRegistrationRequest) error {
	if !req.GetAgreeTnc() {
		return errors.New("terms and conditions must be accepted")
	}
	return nil
}

func (v *RegistrationValidator) validateAndMatch(str, matchRegex, emptyErrorString, noMatchErrorString string) error {
	if v.isEmptyString(str) {
		return errors.New(emptyErrorString)
	}
	if !regexp.MustCompile(matchRegex).MatchString(str) {
		return errors.New(noMatchErrorString)
	}
	return nil
}

func (v *RegistrationValidator) isEmptyString(str string) bool {
	return len(str) < 1
}
