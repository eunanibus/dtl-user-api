package internal

import (
	"bitbucket.org/eunanibus/dtl-user-api/internal/app/dtl-user-api/server/internal/models"
	"bitbucket.org/eunanibus/dtl-user-api/proto/auth"
	"github.com/golang/protobuf/ptypes/empty"
	"golang.org/x/net/context"
)

type UserAPI struct {
	rv  *RegistrationValidator
	kcm *RegistrationValidator
}

func NewUserAPI() *UserAPI {
	return &UserAPI{
		rv: &RegistrationValidator{},
	}
}

func (api *UserAPI) Register(ctx context.Context, req *auth.UserRegistrationRequest) (*empty.Empty, error) {
	if err := api.rv.ValidateRegistrationRequest(req); err != nil {
		return nil, err
	}
	newUser := models.GenerateNewUserFromRequest(req)

	//if session.SessionId != req.SessionId {
	//	log.Errorf("Invalid session for user %s", req.Username)
	//	return nil, errors.New("Session does not exist")
	//}
	//
	//return new(empty.Empty), nil
}
