package main

import (
	"bitbucket.org/eunanibus/dtl-user-api/internal/app/dtl-user-api/server/internal"
	"bitbucket.org/eunanibus/dtl-user-api/proto/auth"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"net"
)

func main() {
	grpcServer := grpc.NewServer()

	usrAPI := internal.NewUserAPI()

	auth.RegisterUserAPIServer(grpcServer, usrAPI)
	defer grpcServer.GracefulStop()

	gRPCServerPort := ":8000"
	listener, err := net.Listen("tcp", gRPCServerPort)
	log.Infof("Server listening on port: %s...", gRPCServerPort)
	if err != nil {
		log.WithError(err).Panic("failed to listen on port - %s", gRPCServerPort)
	}

	if err := grpcServer.Serve(listener); err != nil {
		log.WithError(err).Panic("gRPC server stopped serving requests")
	}
}
