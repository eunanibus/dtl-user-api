# dtl-user-api

A proof-of-concept prototype, extracting the users API to its own isolated service, developed entirely in Golang using protocol buffers. In the future, this library will be compatible with almost all major languages as an importable stub, in addition to the low consumption and cost of resources to deploy.

##### Go Generate
This service is a GRPC prototype whose interface is defined within the `/proto/service.proto` - If this definition changes, you must regenerate the `/auth/service.pb.go` which defines the service to other libraries. To do this, we've included a handy make function:

```make generate```